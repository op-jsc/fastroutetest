<?php

use ObjectivePHP\Application\Config\UrlAlias;
use ObjectivePHP\Package\FastRoute\Config\FastRoute;

return [
        // route aliasing
        new FastRoute('test', '/test', function (){
            echo 'test';
        })

];